"""
--- Day 3: Spiral Memory ---

You come across an experimental new kind of memory stored on an infinite two-dimensional grid.

Each square on the grid is allocated in a spiral pattern starting at a location marked 1 and then counting up while spiraling outward. For example, the first few squares are allocated like this:

17  16  15  14  13
18   5   4   3  12
19   6   1   2  11
20   7   8   9  10
21  22  23---> ...
While this is very space-efficient (no squares are skipped), requested data must be carried back to square 1 (the location of the only access port for this memory system) by programs that can only move up, down, left, or right. They always take the shortest path: the Manhattan Distance between the location of the data and square 1.

For example:

Data from square 1 is carried 0 steps, since it's at the access port.
Data from square 12 is carried 3 steps, such as: down, left, left.
Data from square 23 is carried only 2 steps: up twice.
Data from square 1024 must be carried 31 steps.
How many steps are required to carry the data from the square identified in your puzzle input all the way to the access port?

Your puzzle answer was 480.

--- Part Two ---

As a stress test on the system, the programs here clear the grid and then store the value 1 in square 1. Then, in the same allocation order as shown above, they store the sum of the values in all adjacent squares, including diagonals.

So, the first few squares' values are chosen as follows:

Square 1 starts with the value 1.
Square 2 has only one adjacent filled square (with value 1), so it also stores 1.
Square 3 has both of the above squares as neighbors and stores the sum of their values, 2.
Square 4 has all three of the aforementioned squares as neighbors and stores the sum of their values, 4.
Square 5 only has the first and fourth squares as neighbors, so it gets the value 5.
Once a square is written, its value does not change. Therefore, the first few squares would receive the following values:

147  142  133  122   59
304    5    4    2   57
330   10    1    1   54
351   11   23   25   26
362  747  806--->   ...
What is the first value written that is larger than your puzzle input?

Your puzzle answer was 349975.

Both parts of this puzzle are complete! They provide two gold stars: **

At this point, you should return to your advent calendar and try another puzzle.

Your puzzle input was 347991.
"""

import unittest

def manhattan_distance(data):
    if data == 1:
        return 0
    max_row_bottom = find_rownum(data)
    row_length = len(max_row_bottom) * 2
    col_height = row_length -1
    # print "bottom_row", max_row_bottom
    # print "bottom_row_length", row_length
    # print "column hieght", col_height
    if max_row_bottom[-1] == data:
        # data is on center column bottom half
        # print "vertical steps =>", len(max_row_bottom) - 1
        return len(max_row_bottom) - 1
    else:
        diff = max_row_bottom[-1] - data
        bottom_left_corner = max_row_bottom[-1] - (row_length / 2) + 1
        top_left_corner = bottom_left_corner - col_height + 1
        # top row_length is 2 points less than bottom row length
        top_right_corner = top_left_corner - row_length + 2
        # right outer column is one pint short of left outer column
        # we already have on value. so.
        bottom_right_corner = top_right_corner - col_height + 2
        # print "top left =>", top_left_corner
        # print "top_right =>", top_right_corner
        # print "bottom_left", bottom_left_corner
        # print "bottom right=>", bottom_right_corner
        if data >= bottom_left_corner:
            # data & our value are on same row
            # print "horizontal steps =>", diff
            # print "vertical steps =>", len(max_row_bottom) - 1
            return (len(max_row_bottom) - 1) + diff
        elif bottom_left_corner > data and top_left_corner < data:
            # data is on left outer column
            mid_val = top_left_corner + (col_height -1) / 2
            # print mid_val
            if data == mid_val:
                # data is on y axis zero
                # print "horizontal steps =>", (row_length / 2) - 1
                return (row_length / 2) - 1
            else:#if data < mid_val:
                # data is in negative y quadrant
                # print "vertical steps =>", mid_val - data
                # print "horizontal steps =>", (row_length / 2) - 1
                return abs(mid_val - data) + ((row_length / 2) - 1)
            # elif data > mid_val:
            #     # data is in positive y quadrant
            #     print "vertical steps =>", data - mid_val
            #     print "horizontal steps =>", (row_length / 2) - 1
            #     return (data - mid_val) + ((row_length / 2) - 1)
        elif top_left_corner >= data and top_right_corner <= data:
            mid_val = top_right_corner + ((top_left_corner - top_right_corner) / 2)
            # print mid_val
            if data == mid_val:
                # data is on centre col
                # print "vertival steps=>", col_height / 2
                return col_height / 2
            else: #if data < mid_val and data > top_right_corner:
                # data is on positive x
                # print "horizontal steps =>", mid_val - data
                # print "vertical steps =>", col_height / 2
                return abs(mid_val - data) + (col_height / 2)
            # elif data > mid_val and data < top_left_corner:
            #     print "horizontal steps =>", data - mid_val
            #     print "vertical steps =>", col_height / 2
            #     return (data - mid_val) + (col_height / 2)
        elif top_right_corner >= data and bottom_right_corner <= data:
            mid_val = bottom_right_corner + ((top_right_corner - bottom_right_corner) / 2)
            # print mid_val
            if data == mid_val:
                # print "horizontal steps =>", (row_length / 2) - 1
                return (row_length / 2) - 1
            else:
                # print "vertival steps =>", abs(mid_val - data)
                # print "horizontal steps =>", (row_length / 2) - 1
                return abs(mid_val - data) + (row_length / 2) - 1
    return "Not Implemented"

def find_rownum(data):
    """
        for the center column the row values can be determined as follows
        (7 * (row_num - 1)) + (row_num -2)
        check the spiral for this pattern
    """
    center_col_bottom_half = [1]
    value = 1
    while value < data:
        row_num = len(center_col_bottom_half) + 1
        # print row_num
        value = value + (7 * (row_num - 1) + (row_num -2))
        # print value
        center_col_bottom_half.append(value)
    return center_col_bottom_half


class TestManhattanDistance(unittest.TestCase):
    def test_case1(self):
        self.assertEqual(manhattan_distance(1), 0)

    def test_case2(self):
        self.assertEqual(manhattan_distance(12), 3)

    def test_case3(self):
        """x axis 0, positive y axis"""
        self.assertEqual(manhattan_distance(23), 2)

    def test_case4(self):
        self.assertEqual(manhattan_distance(1024), 31)

    def test_case5(self):
        """x axis 0 positive y axis"""
        self.assertEqual(manhattan_distance(77), 4)

    def test_case6(self):
        """negative x axis and positive y axis on the bottom row"""
        self.assertEqual(manhattan_distance(75), 6)

    def test_case7(self):
        """negative x axis and positive y axis on outer most col"""
        self.assertEqual(manhattan_distance(70), 5)

    def test_case8(self):
        """negative x axis and positive y axis on outer most col and on bottow row"""
        self.assertEqual(manhattan_distance(73), 8)

    def test_case9(self):
        """negative x axis and positive y axis on outer most col and on center row"""
        self.assertEqual(manhattan_distance(69), 4)

    def test_case10(self):
        """left outer most col above center row"""
        self.assertEqual(manhattan_distance(68), 5)

    def test_case11(self):
        """top most row center col"""
        self.assertEqual(manhattan_distance(96), 5)

    def test_case12(self):
        """top most row negative x"""
        self.assertEqual(manhattan_distance(97), 6)

    def test_case13(self):
        """top most row positive x"""
        self.assertEqual(manhattan_distance(95), 6)

    def test_case14(self):
        """bottom left corner"""
        self.assertEqual(manhattan_distance(111), 10)

    def test_case15(self):
        """top left corner"""
        self.assertEqual(manhattan_distance(101), 10)

    def test_case16(self):
        """top right corner"""
        self.assertEqual(manhattan_distance(91), 10)

    def test_case17(self):
        """bottom right corner"""
        self.assertEqual(manhattan_distance(82), 9)

    def test_case_main(self):
        print "part 1 =>", manhattan_distance(347991)


def generate_sum_spiral(limit):
    spiral = [
        [1]
    ]

    center = (1, 1)
    current_row_idx = 0
    current_col_idx = 0
    value = 0
    count = 0
    direction = "down"
    while limit > value:
        direction_change = False
        if direction == "down" and len(spiral[-1]) == 1:#len(spiral) == len(spiral[-1]):
            direction = "right"
            direction_change = True
        elif direction == "right" and len(spiral) < len(spiral[-1]):
            direction = "up"
            direction_change = True
        elif direction == "up" and len(spiral) == len(spiral[-1]) and spiral[0][-1] != 0:
            direction = "left"
            direction_change = True
        elif direction == "left" and len(spiral[0]) > len(spiral[1]):
            direction = "down"
            direction_change = True

        # print "direction =>", direction

        if direction == "right":
            current_col_idx = current_col_idx + 1
        elif direction == "up":
            current_row_idx = current_row_idx - 1
            if current_row_idx < 0:
                spiral.insert(0, [0 for x in range(current_col_idx + 1)])
                current_row_idx = 0
            else:
                spiral[current_row_idx].append(0)
        elif direction == "left":
            current_col_idx = current_col_idx - 1
        elif direction == "down":
            current_row_idx = current_row_idx + 1
            current_col_idx = 0
            if direction_change:
                for idx, row in enumerate(spiral):
                    if idx == 0:
                        continue
                    row.insert(0, 0)
            try:
                spiral[current_row_idx].insert(0, 0)
                spiral[current_row_idx].pop(0)
            except IndexError:
                spiral.append([0])

        direction_change = False

        # print "spiral @ start"
        # for row in spiral:
        #     print row

        # print "row =>", current_row_idx
        # print "col =>", current_col_idx

        def valid(x):
            return x[0] if x[0] >= 0 else None, x[1] if x[1] >= 0 else None
        def down(x):
            return x[0] if x[0] > len(spiral) else None, x[1] if x[1] <= len(spiral) else None
        tl  = valid((current_row_idx -1, current_col_idx -1))
        tp  = valid((current_row_idx -1, current_col_idx))
        tr  = valid((current_row_idx -1, current_col_idx + 1))
        lft = valid((current_row_idx, current_col_idx - 1))
        cur = valid((current_row_idx, current_col_idx))
        rt  = valid((current_row_idx, current_col_idx + 1))
        bl  = valid((current_row_idx + 1, current_col_idx - 1))
        bt  = valid((current_row_idx + 1, current_col_idx))
        br  = valid((current_row_idx + 1, current_col_idx + 1))
        # print "co-ordinates"
        # print tl, tp, tr
        # print lft, cur, rt
        # print bl, bt, br


        try:
            top_left = spiral[tl[0]][tl[1]]
        except:
            top_left = 0
        try:
            top = spiral[tp[0]][tp[1]]
        except:
            top = 0
        try:
            top_right = spiral[tr[0]][tr[1]]
        except:
            top_right = 0
        try:
            right = spiral[rt[0]][rt[1]]
        except:
            right = 0
        try:
            bottom_right = spiral[br[0]][br[1]]
        except:
            bottom_right = 0
        try:
            bottom = spiral[bt[0]][bt[1]]
        except:
            bottom = 0
        try:
            bottom_left = spiral[bl[0]][bl[1]]
        except:
            bottom_left = 0
        try:
            left = spiral[lft[0]][lft[1]]
        except:
            left = 0
        # print "surrounding values"
        # print top_left, top, top_right
        # print left, "?", right
        # print bottom_left, bottom, bottom_right
        value = top_left + top + top_right + right + bottom_right + bottom + bottom_left + left
        # print value
        if direction == "left" and spiral[current_row_idx][current_col_idx] != 0:
            spiral[current_row_idx].insert(0, value)
        elif direction in ("left", "up", "down"):
            spiral[current_row_idx][current_col_idx] = value
        else:
            spiral[current_row_idx].append(value)
        # print "spiral"
        # for row in spiral:
        #     print row
        # print "*" * 20

        count += 1
        if count == 15:
            # break
            pass
    return value


if __name__ == '__main__':
    # unittest.main()
    print "part 2 =>", generate_sum_spiral(347991)
