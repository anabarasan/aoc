"""Day 10 - Knot Hash"""
from collections import deque

NUMBERS = list(range(256))
LENGTHS = '183,0,31,146,254,240,223,150,2,206,161,1,255,232,199,88'

# NUMBERS = [0, 1, 2, 3, 4]
# LENGTHS = '3,4,1,5'

INT_SIZES = [int(x) for x in LENGTHS.split(",")]
SKIPSIZE = 0
CURRENTPOSITION = 0

def knot_hash(hashlist, sizes, pos=0, skip=0):
    """ calculate knot hash """
    for size in sizes:
        hashlist.rotate(-pos)
        circular_list = list(hashlist)
        circular_list[:size] = reversed(circular_list[:size])
        hashlist = deque(circular_list)
        hashlist.rotate(pos)

        pos += (size + skip)
        if pos >= len(hashlist):
            pos = pos % len(hashlist)
        skip += 1
    return hashlist, pos, skip

KNOTHASH, FINAL_POS, FINAL_SKIPSIZE = knot_hash(deque(NUMBERS), INT_SIZES)
print "part 1 =>", KNOTHASH[0] * KNOTHASH[1]


# LENGTHS = 'AoC 2017'


SECONDHASH = deque(NUMBERS)
SKIPSIZE = 0
CURRENTPOSITION = 0
ASCII_SIZES = [ord(x) for x in LENGTHS] + [17, 31, 73, 47, 23]

for _ in range(64):
    SECONDHASH, CURRENTPOSITION, SKIPSIZE = knot_hash(SECONDHASH, ASCII_SIZES,
                                                      CURRENTPOSITION, SKIPSIZE)

SPARSEHASH = list(SECONDHASH)
DENSEHASH = []

for block in range(0, len(SPARSEHASH), 16):
    hashed = 0
    hash_group = SPARSEHASH[block : block+16]
    for item in hash_group:
        hashed ^= item
    DENSEHASH.append(hashed)

HEXKNOTHASH = "".join([format(x, '02x') for x in DENSEHASH])
print "part 2 =>", HEXKNOTHASH
