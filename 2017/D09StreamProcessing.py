"""Day 09 - Stream Processing"""
GARBAGE = False
IGNORE = False
ESCAPE_IGNORE = ('<', '>', '!')


# stream = '{{{},{},{{}}}}' # 6, 16
# stream = '{<{},{},{{}}>}' #1, 1
# stream = '{{<!>},{<!>},{<!>},{<a>}}' # 2, 3
# stream = '{{<!!>},{<!!>},{<!!>},{<!!>}}' # 5, 9

with open('stream.txt') as ip:
    stream = ip.readline()

level = 0
score = 0
opened = 0
closed = 0
GARBAGE_COUNT = 0

for char in stream:
    if GARBAGE:
        if IGNORE:
            IGNORE = False
        elif char == '!':
            IGNORE = True
        elif char == '>':
            GARBAGE = False
        else:
            GARBAGE_COUNT += 1
    else:
        if char == '{':
            opened += 1
            level += 1
        elif char == '}':
            closed += 1
            opened -= 1
            score += level
            level -= 1
        elif char == '<':
            GARBAGE = True

print 'no of groups =', closed
print 'score =', score # part 1
print 'garbage score =', GARBAGE_COUNT # part 2
