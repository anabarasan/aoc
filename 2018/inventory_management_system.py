"""aoc 2018 day 02 - Inventory Management System"""
#!/usr/bin/env python3
# pylint: disable=invalid-name

from collections import Counter
from itertools import product

def get_diff_positions(string1, string2):
    """get the positions where the string chars are different"""
    return [i for i, (left, right) in enumerate(zip(string1, string2)) if left != right]

def part1(box_ids):
    """calculate checksum of box ids"""
    twos, threes = 0, 0
    for box_id in box_ids:
        counts = [cnt for char, cnt in Counter(box_id).most_common()]
        if 2 in counts:
            twos += 1
        if 3 in counts:
            threes += 1
    return twos * threes

def part2(box_ids):
    """correct box ids common letters"""
    for idx, _ in enumerate(box_ids):
        compare_list = box_ids[:]
        compare_list.pop(idx) # remove the current test id, not that it matters though
        for combination in product(box_ids, compare_list):
            diff_pos = get_diff_positions(*combination)
            if len(diff_pos) == 1: # possible fabric box
                return ''.join([ch for idx, ch in enumerate(combination[0]) if idx != diff_pos[0]])
    return None


if __name__ == '__main__':
    with open('inventory_management_system.txt') as ip:
        ids = [line.strip() for line in ip]

    print("checksum => ", part1(ids)) # should print 8118
    print("letters common in correct box ids =>", part2(ids)) # jbbenqtlaxhivmwyscjukztdp
