"""aoc 2018 day 4 - Repose Record"""
#!/usr/bin/env python3
#pylint: disable=invalid-name

def sort_data_key_gen(rec):
    """generate timestamp with which data is to be sorted"""
    start = rec.find('[')
    end = rec.find(']')
    timestamp = rec[start+1 : end-start]
    timestamp = timestamp.replace('-', '').replace('-', '').replace(':', '')
    timestamp = timestamp.replace(' ', '')
    return timestamp

def get_guard_duty_data():
    """read input data from the input file"""
    with open('repose_record.txt') as ip:
        records = [line.strip() for line in ip]
        records.sort(key=sort_data_key_gen)
    return records

def compute_sleep_data(data):
    """calculate the amount of time a guard sleeps"""
    duty_data = {}
    guard_id = None
    sleeps_at = []
    awake_at = []
    for record in data:
        details = record.split(' ')
        if 'Guard' in record:
            if guard_id is not None:
                total_sleep_time = 0
                sleep_data = ['.'] * 60
                for idx, value in enumerate(sleeps_at):
                    start = value # sleeps_at[idx]
                    end = awake_at[idx]
                    total_sleep_time += (end-start)
                    while start < end:
                        sleep_data[start] = '#'
                        start += 1
                duty_data[guard_id]['total_sleep_time'] = total_sleep_time
                duty_data[guard_id]['sleep_data'].append(sleep_data)
            guard_id = details[3].replace('#', '')
            if guard_id not in duty_data:
                duty_data[guard_id] = {'total_sleep_time': 0,
                                       'sleep_data': []}
        if 'falls asleep' in record:
            sleeps_at.append(int(details[1].split(':')[1].replace(']', '')))
        if 'wakes up' in record:
            awake_at.append(int(details[1].split(':')[1].replace(']', '')))
