"""aoc 2018 day 03 - No Matter How You Slice It"""
#!/usr/bin/env python3
#pylint: disable=invalid-name

def read_input():
    """read the input file split it to details and yield line by line"""
    with open('no_matter_how_you_slice_it.txt') as ip:
        for line in ip:
            id_, _, coordinates, dimensions = line.strip().split()
            id_ = int(id_.replace('#', ''))
            coordinates = tuple([int(x) for x in coordinates[:-1].split(',')])
            dimensions = tuple([int(x) for x in dimensions.split('x')])
            yield id_, coordinates, dimensions

def get_overlaps():
    """check if there are any overlaps"""
    claimed = set()
    filled = set()
    for detail in read_input():
        _, coordinates, dimensions = detail
        x, y = coordinates
        for w in range(dimensions[0]):
            for t in range(dimensions[1]):
                point = (x+w, y+t)
                if point in claimed:
                    filled.add(point)
                else:
                    claimed.add(point)
    return filled

def get_no_overlaps():
    """get the non overlapping claim"""
    overlaps = get_overlaps()
    for detail in read_input():
        _, coordinates, dimensions = detail
        x, y = coordinates
        has_overlap = False
        for w in range(dimensions[0]):
            for t in range(dimensions[1]):
                point = (x+w, y+t)
                if point in overlaps:
                    has_overlap = True
                    break
            if has_overlap:
                break
        if not has_overlap:
            return _
    return None

if __name__ == '__main__':
    print("overlapping square inches of fabric => ", len(get_overlaps())) # 105231
    print("non overlaping square inch of fabric =>", get_no_overlaps()) # 164
