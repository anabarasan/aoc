"""aoc 2018 day 01 - chronal_calibration"""
#!/usr/bin/env python3
# pylint: disable=invalid-name

from itertools import cycle

with open('chronal_calibration.txt') as ip:
    frequency_change_list = [int(line) for line in ip]

def part1():
    """part 1 solution"""
    return sum(frequency_change_list)

def part2():
    """part2 solution"""

    # read that sets are faster than lists
    # because sets use hash to search while list use values to search
    resulting_frequencies = set()
    current_frequency = 0

    # TIL using cycle from itertools we can avoid the while to cycle through
    # the for loop continuosly
    for change in cycle(frequency_change_list):
        current_frequency += change
        if current_frequency in resulting_frequencies:
            return current_frequency
        resulting_frequencies.add(current_frequency)

if __name__ == '__main__':
    print("resulting_frequency => ", part1()) # should print 406
    print("first repeating frequency => ", part2()) # should print 312
